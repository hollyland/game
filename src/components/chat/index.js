import React, { useEffect } from "react";

import useChat from "./useChat";
import {Button, Card, CardTitle, Col, Form, FormGroup, Input, Label, ListGroup, ListGroupItem, Row} from "reactstrap";
import { CHAT_MESSAGES } from "./styles";

const ChatRoom = ({gameId, username}) => {
    const {messages, sendMessage} = useChat(gameId, username);
    const [newMessage, setNewMessage] = React.useState('');

    const handleNewMessageChange = (e) => {
		e.preventDefault();
        setNewMessage(e.target.value);
    };

    const handleSendMessage = (e) => {
		e.preventDefault();
        if (newMessage != '') {
            sendMessage(newMessage, gameId);
            setNewMessage('');
        }
	};
	
	useEffect(() => {
		let messageBody = document.querySelector('#messages');
		messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
	});

    return (
        <Card body className="mt-4">
            <CardTitle className="text-uppercase text-muted mb-4">Chat</CardTitle>
			<Row>
				<Col>
					<ListGroup id="messages" style={CHAT_MESSAGES}>
						{messages.map((message, i) => (
							<ListGroupItem key={i}>
								<b>{message.username}: </b>
								{message.body}
							</ListGroupItem>
						))}
					</ListGroup>
				</Col>
			</Row>
			<Row className="mt-2">
				<Col>
					<Form onSubmit={handleSendMessage}>
						<FormGroup>
							<Input
								type="text"
								name="text"
								id="textMessage"
								placeholder="Écris ton message"
								value={newMessage}
								onChange={handleNewMessageChange}
							/>
						</FormGroup>
						<Button type="submit" block size="sm" color="navy">Envoyer</Button>
					</Form>
				</Col>
			</Row>
        </Card>
    );
};

export default ChatRoom;

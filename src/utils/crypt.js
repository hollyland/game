import CryptoJS from 'crypto-js';

const secret = 'RTPDT-IWAPI-WSTVP-TJNDD-KJRHL-RQFRW-PLMHN-DETTR-QBQXS-YGQOT-ABPRF-BMXRT';

export function encrypt(value) {
  return CryptoJS.AES.encrypt(value, secret).toString();
}

export function decrypt(value) {
  return CryptoJS.AES.decrypt(value, secret).toString(CryptoJS.enc.Utf8);
}

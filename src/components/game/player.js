import React from 'react';
import { Card, CardImg, CardBody, Col } from 'reactstrap';
import img_user from '../../assets/img/default-user.png'

const Player = ({ player }) => {
    return (
        <Col sm="1">
            <Card className="bg-navy text-white">
                <CardImg top width="100%" src={img_user} alt="user" />
                <CardBody style={{ padding: '0px' }} className="text-center">
                    <h5 className="text-white mt-1">{player.username}</h5>
                    <hr style={{ marginTop: '-.5rem', marginBottom: '.3rem' }} />
                    <h6 style={{ color: '#FFD700' }}>{player.coins} <small>pièces</small></h6>
                </CardBody>
            </Card>
        </Col>
    )
};

export default Player;

import { SET_PLAYER } from '../types';

const initialState = {
  username: '',
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_PLAYER: {
      const { user } = payload;
      return { ...state, username: user };
    }
    default: {
      return state;
    }
  }
}

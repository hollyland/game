import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import store from './store/store'
import Game from "./game";

/* Importation du CSS */
import './assets/scss/argon-design-system-react.scss';

/* Récupération de l'élément HTML dans lequel est injecté l'application compilée */
const wrapper = document.querySelector('hollyland-mascarade');

/* Temporaire : attendre que le pseudo soit indiqué dans l'alerte pour débuter */
const waitFor = setInterval(() => {
  let user = wrapper.getAttribute('user');
  if (user !== '' || user !== undefined) {
    clearInterval(waitFor);
    ReactDOM.render(
      <Provider store={store}>
        <Game user={user} />
      </Provider>
      , wrapper
    );
  }
}, 100);

import React, { useState, useEffect } from 'react';
import Player from './player';
import { connect } from 'react-redux';
import { Row, Col, Card } from 'reactstrap';
import courtImg from '../../assets/img/mascarade_tribunal.png';
import { gameSocket, getGameCards, getUnusedCards } from '../../utils';
import ChooseCardModal from './modals/chooseCardModal';
import VoteModal from './modals/voteModal';
import VoteDoneModal from './modals/voteDoneModal';
import ShowCardModal from './modals/showCardModal';
import { CHANGE_GAME_STATUS, SET_WINNERS, UPDATE_PLAYERS } from '../../store/types';
import ChatRoom from '../chat';
import GameRoundInfo from './gameRoundInfo';
import SwitchCardModal from './modals/switchCardModal';
import FirstPlayerModal from './modals/firstPlayerModal';
import StartModal from './modals/startModal';
import PlayerLeftModal from './modals/playerLeftModal';

const GameBoard = ({ game, username, dispatch }) => {
    /* Définition des états du composant */
    const [startScreen, setStartScreen] = useState(true);
    const [players, setPlayers] = useState([]);
    const [banqueCoins, setBanqueCoins] = useState(0);
    const [courtCoins, setCourtCoins] = useState(0);
    const [myCard, setMyCard] = useState({});
    const [firstPlayer, setFirstPlayer] = useState(null);
    const [playingPlayer, setPlayingPlayer] = useState({});
    const [round, setRound] = useState(0);
    const [canStart, setCanStart] = useState(false);
    const [unusedCards, setUnusedCards] = useState([]);
    const [playerLeft, setPlayerLeft] = useState('');
    const [action, setAction] = useState(0);
    const [voteInProgress, setVoteInProgress] = useState(false);
    const [vote, setVote] = useState({});
    const [voteDone, setVoteDone] = useState(false);
    const [voteResult, setVoteResult] = useState({});
    const [exchangeRequired, setExchangeRequired] = useState(false);
    const [previousPlayerAction, setPreviousPlayerAction] = useState(false);

    /* Constantes */
    const nbPlayers = game.players.length;

    /* Setter les états quand le composant est prêt */
    useEffect(() => {
        setBanqueCoins(194 - (game.startCoins * nbPlayers));
        setPlayers(game.players);
        setMyCard(game.players.find(player => player.username === username).card);
        setUnusedCards(getUnusedCards(game.players, getGameCards(game.players.length)));
    }, []);

    /* On prévient les autres joueurs qu'un joueur a quitté la partie */
    useEffect(() => {
        const handleBeforeunload = (event) => {
            event.preventDefault();
            gameSocket.emit('player_left_game', game.gameId, username);
        };

        window.addEventListener('beforeunload', handleBeforeunload);
        return () => window.removeEventListener('beforeunload', handleBeforeunload);
    }, []);

    /* Ecouter le serveur socket et mettre à jour les informations quand c'est au tour d'un nouveau joueur */
    useEffect(() => {
        gameSocket.on('next_player', (player, game, customData) => {
            /* On met à jour les états du composant */
            setFirstPlayer(game.firstPlayer);
            setMyCard(game.players.find(player => player.username === username).card);
            setUnusedCards(getUnusedCards(game.players, getGameCards(game.players.length)));
            setPlayers(game.players);
            setPlayingPlayer(player);
            setRound(game.round);
            setBanqueCoins(game.banqueCoins);
            setCourtCoins(game.courtCoins);
            setPreviousPlayerAction(false);
            if (customData.requestExchangeList?.includes(username)) setExchangeRequired(true);
            if (customData.prevPlayerAction !== null) setPreviousPlayerAction(customData.prevPlayerAction);
        });

        gameSocket.on('player_left_game', (username) => {
            setPlayerLeft(username);
        });
    }, []);

    /* Ecouter le serveur socket et mettre à jour les informations quand la partie est terminée */
    useEffect(() => {
        gameSocket.on('game_ended', (gameInfos, gameWinners) => {
            dispatch({ type: SET_WINNERS, payload: { winners: gameWinners } });
            dispatch({ type: UPDATE_PLAYERS, payload: { players: gameInfos.players } });
            dispatch({ type: CHANGE_GAME_STATUS, payload: { gameStatus: 3 } });
        });

        return () => {
            gameSocket.off('game_ended');
        };
    }, []);

    /* Ecouter le serveur socket et mettre à jour les informations quand un joueur annonce un personnage */
    useEffect(() => {
        gameSocket.on('card_announcement', (voteObj) => {
            setVote(voteObj);
            setVoteInProgress(true);
        });

        return () => {
            gameSocket.off('card_announcement');
        };
    }, []);

    /* Ecouter le serveur socket et mettre à jour les informations quand les votes de contestations sont terminés */
    useEffect(() => {
        gameSocket.on('vote_done', (votes, initialVote, players) => {
            const contestants = votes.filter(({ choice }) => choice === 2).map(vote => vote.player);
            const isDisputed = contestants.length === 0 ? false : true;
            setVoteInProgress(false);
            setVoteResult({ voteWinner: getVoteWinner(initialVote, players, isDisputed, contestants), contestants, initialVote });
            setVoteDone(true);
        });

        return () => {
            gameSocket.off('vote_done');
        };
    }, []);

    /* Gestion de l'action "Voir ma carte" */
    const handleOnShowMyCard = () => {
        let updatedPlayers = players;
        let prevPlayerAction = { action: 2, username };

        updatedPlayers.find(player => player.username === username).score += 90;
        setAction(0);

        /* Envoyer au socket les informations mises à jour pour que les autres joueurs les recoivent */
        gameSocket.emit('get_next_player', game.gameId, { updatedPlayers }, false, { prevPlayerAction });
    };

    /* Emet l'évènement déclencheur pour lancer le vote */
    const handleOnCardAnnouncement = (card) => {
        gameSocket.emit('request_card_announcement', game.gameId, players.find(player => player.username === username), card);
    };

    const getVoteWinner = (initialVote, players, isDisputed, contestants) => {
        let playerWithCard = null;
        if (isDisputed) {
            players.forEach(player => {
                if (player.card.name === initialVote.card.name && (contestants.map(contestant => contestant.username).includes(player.username) || player.username === initialVote.player.username)) {
                    playerWithCard = player;
                }
            });
            return playerWithCard;
        }
        return players.filter(({ username }) => username === initialVote.player.username)[0];
    };

    /* Permet de remettre les états par défaut à la fin de l'action d'échan */
    const resetStatesForCardAnnouncementAction = () => {
        setVote({});
        setVoteDone(false);
        setVoteResult({});
        setAction(0);
    };

    /* Gestion de l'action "Echanger ma carte" */
    const resetStatesForSwitchCardAction = (choosenCard) => {
        /* Mettre à jour la carte du joueur */
        setMyCard(choosenCard);
        setAction(0);
        setExchangeRequired(false);
    };

    return (
        <div className="w-100">
            {/* Message d'alerte quand un joueur est parti de la partie */}
            {playerLeft && <PlayerLeftModal
                isOpen={Boolean(playerLeft)}
                playerLeft={playerLeft}
                timer={30}
            />}

            {/* Affiche les cartes de chaque joueur en début de partie */}
            <StartModal
                isOpen={startScreen}
                players={players}
                timer={12}
                onTimerDone={() => {
                    setStartScreen(false);
                    if (game.createdBy === username) gameSocket.emit('get_next_player', game.gameId, { updatedPlayers: players, updatedBanqueCoins: banqueCoins }, true);
                }}
            />

            {/* Affiche le nom du premier joueur à jouer */}
            {firstPlayer && <FirstPlayerModal
                isOpen={!canStart}
                username={firstPlayer.username}
                timer={5}
                onTimerDone={() => { setRound(1); setCanStart(true); }}
            />}

            {/* Affiche la fenêtre pour réaliser l'action d'échanger sa carte */}
            {((round === 1 && canStart) || action === 1) && playingPlayer.username === username && <SwitchCardModal
                isOpen={(round === 1 && canStart) || action === 1}
                myCard={myCard}
                players={players}
                unusedCards={unusedCards}
                actionDone={(choosenCard) => resetStatesForSwitchCardAction(choosenCard)}
            />}

            {/* Affiche la fenêtre pour réaliser l'action de regarder sa carte */}
            {action === 2 && <ShowCardModal
                isOpen={action === 2}
                card={myCard}
                timer={5}
                onTimerDone={() => handleOnShowMyCard()}
            />}

            {/* Affiche la fenêtre pour réaliser l'action d'annoncer un personnage */}
            {action === 3 && !voteInProgress && !voteDone && <ChooseCardModal
                isOpen={action === 3}
                nbPlayers={nbPlayers}
                onChoice={(card) => handleOnCardAnnouncement(card)}
            />}

            {/* Affiche la fenêtre de vote pour contester ou non l'annonce d'un personnage */}
            {voteInProgress && <VoteModal
                isOpen={voteInProgress}
                isAnnouncer={vote.player?.username === username}
                vote={vote}
                nbPlayers={nbPlayers}
                players={players}
            />}

            {/* Affiche la fenêtre qui affiche le résultat des votes de contestation */}
            {voteDone && <VoteDoneModal
                isOpen={voteDone}
                result={voteResult}
                players={players}
                courtCoins={courtCoins}
                banqueCoins={banqueCoins}
                actionDone={() => resetStatesForCardAnnouncementAction()}
                timer={10}
            />}

            {/* Affichage du plateau de jeu */}
            {canStart && <div>
                <Row className="justify-content-center align-items-end mb-5">
                    <Col sm="4" md="3">
                        <ChatRoom gameId={game.gameId} username={username} />
                    </Col>
                    <Col className="px-5" sm="4" md="6">
                        <Row className="justify-content-center align-items-center">
                            <img src={courtImg} className="img-fluid" width="75%" />
                        </Row>
                        <Row>
                            <Card body className="mt-4">
                                <Row className="justify-content-center align-items-center">
                                    <Col>
                                        <h5 className="text-uppercase font-weight-bold text-center mb-0"><small>ROUND N°</small></h5>
                                        <h4 className="font-weight-bold text-center">{round}</h4>
                                    </Col>
                                    <Col>
                                        <h5 className="text-uppercase font-weight-bold text-center mb-0"><small>BANQUE</small></h5>
                                        <h4 className="font-weight-bold text-center">{banqueCoins} <small className="d-none d-lg-block">pièces</small></h4>
                                    </Col>
                                    <Col>
                                        <h5 className="text-uppercase font-weight-bold text-center mb-0"><small>TRIBUNAL</small></h5>
                                        <h4 className="font-weight-bold text-center">{courtCoins} <small className="d-none d-lg-block">pièces</small></h4>
                                    </Col>
                                </Row>
                            </Card>
                        </Row>
                    </Col>
                    <Col sm="4" md="3">
                        <GameRoundInfo
                            playingPlayer={playingPlayer}
                            action={action}
                            round={round}
                            exchangeRequired={exchangeRequired}
                            previousPlayerAction={previousPlayerAction}
                            onClick={(action) => setAction(action)}
                        />
                    </Col>
                </Row>
                <Row className="justify-content-center mt-5 pb-5">
                    {players.map((player, index) => (
                        <Player key={index} player={player} />
                    ))}
                </Row>
            </div>}
        </div>
    );
};

const selector = ({ game, player: { username } }) => ({ game, username });
export default connect(selector)(GameBoard);

import {useEffect, useState} from "react";
import {gameSocket} from '../../utils';

const useChat = (gameId, username) => {
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        let mounted = true;
        if (mounted) {
            gameSocket.on("newChatMessage", (message) => {
                const incomingMessage = {
                    ...message,
                    ownedByCurrentUser: message.senderId === gameSocket.id
                };
                setMessages((messages) => [
                    ...messages,
                    incomingMessage
                ]);
            });
        }

        return() => {
            mounted = false;
        };
    }, [gameId]);

    const sendMessage = (messageBody, gameId) => {
        gameSocket.emit("newChatMessage", {
            body: messageBody,
            username: username,
            senderId: gameSocket.id,
            gameId: gameId
        });
    };

    return {messages, sendMessage};
};

export default useChat;

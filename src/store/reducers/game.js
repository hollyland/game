import { NEW_GAME, CHANGE_GAME_STATUS, BACK_TO_GAME, UPDATE_PLAYERS, SET_WINNERS } from '../types';
/* LINT */

const initialState = {
    gameStatus: 0,
    gameId: '',
    name: '',
    password: null,
    maxPlayers: 4,
    startCoins: 6,
    createdBy: '',
    players: [],
    winners: []
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case NEW_GAME: {
            const {
                gameStatus,
                gameId,
                name,
                password,
                maxPlayers,
                startCoins,
                createdBy
            } = payload;
            return {
                ...state,
                gameStatus,
                gameId,
                name,
                password,
                maxPlayers,
                startCoins,
                createdBy
            };
        }
        case BACK_TO_GAME: {
            const {
                gameStatus,
                gameId,
                name,
                password,
                maxPlayers,
                startCoins,
                createdBy,
                players
            } = payload;
            return {
                ...state,
                gameStatus,
                gameId,
                name,
                password,
                maxPlayers,
                startCoins,
                createdBy,
                players
            };
        }
        case CHANGE_GAME_STATUS: {
            const { gameStatus } = payload;
            return { ...state, gameStatus };
        }
        case UPDATE_PLAYERS: {
            const { players } = payload;
            return { ...state, players };
        }
        case SET_WINNERS: {
            const { winners } = payload;
            return { ...state, winners };
          }
        default: {
            return state;
        }
    }
}

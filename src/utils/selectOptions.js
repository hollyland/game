import React from 'react';

/* Définir les options disponibles (pour la création de partie) */
export default function setSelectOptions(from, to) {
  let select = []
  for (let i = from; i <= to; i++) {
    select.push(<option key={i}>{i}</option>)
  }
  return select
}

export const CHAT_MESSAGES = {
    height: '10rem',
    maxHeight: '10rem',
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch'
};

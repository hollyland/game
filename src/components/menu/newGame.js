import React, { useState } from 'react';
import Axios from 'axios';
import { Form, Row, Col, FormGroup, Input, Label, FormText, Button, CardBody, Card } from 'reactstrap';
import { setSelectOptions, generateGameId } from '../../utils';
import { API_TOKEN, API_URL } from '../../config';
import { connect } from 'react-redux';
import { NEW_GAME } from '../../store/types';
import { gameSocket, encrypt } from '../../utils';

const NewGame = ({ username, dispatch }) => {
    /* Définition des états du composant */
    const [isValid, setIsValid] = useState(false);
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [maxPlayers, setMaxPlayers] = useState(4);
    const [startCoins, setStartCoins] = useState(6);

    /* Soumission de la partie */
    const handleSubmit = () => {
        /* Récupération des options de la partie à créer */
        const gameOptions = {
            gameId: generateGameId(),
            name,
            maxPlayers,
            startCoins,
            password: password ? encrypt(password) : '',
            createdBy: username
        };

        /* Appel à l'API pour créer la partie */
        Axios({
            url: API_URL + 'new-game',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-API-KEY": API_TOKEN
            },
            data: JSON.stringify(gameOptions)
        }).then(() => {
            /* On envoie au socket que la partie commence et on met à jour le store */
            gameSocket.emit('init_new_game', { username, gameOptions });
            gameSocket.on('game_created', (gameId) => {
                dispatch({
                    type: NEW_GAME,
                    payload: {
                        gameStatus: 1,
                        gameId,
                        name: gameOptions.name,
                        password: gameOptions.password,
                        maxPlayers: gameOptions.maxPlayers,
                        startCoins: gameOptions.startCoins,
                        createdBy: username
                    }
                });
            });
        }).catch((error) => {
            console.log(error);
        });
    };

    return (
        <div>
            <Card body className="mb-2">
                <h1 className="display-4 text-uppercase text-center mb-0">Créer une partie</h1>
            </Card>
            <Card body>
                <Form>
                    <Row className="mb-4">
                        <Col md="6">
                            <FormGroup>
                                <Label className="text-uppercase font-weight-bold" htmlFor="selectPlayerNb">Nom de la partie</Label>
                                <Input
                                    className="form-control-alternative"
                                    type="text"
                                    value={name}
                                    onChange={e => {
                                        setName(e.target.value);
                                        e.target.value === '' ? setIsValid(false) : setIsValid(true);
                                    }}
                                />
                                <FormText color="muted">
                                    Le nom de la partie sera affiché dans la recherche de partie, elle permet d'identifier votre partie.
                                </FormText>
                            </FormGroup>
                        </Col>
                        <Col md="6">
                            <FormGroup>
                                <Label className="text-uppercase font-weight-bold" htmlFor="password">Mot de passe<small> facultatif</small></Label>
                                <Input
                                    id="password"
                                    className="form-control-alternative"
                                    type="text"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                />
                                <FormText color="muted">
                                    Vous pouvez définir un mot de passe pour rendre votre partie privée. Laissez vide si vous souhaitez rendre la partie publique.
                        </FormText>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row className="mb-4">
                        <Col md="6">
                            <FormGroup>
                                <Label className="text-uppercase font-weight-bold" htmlFor="selectPlayerNb">Nombre de joueurs</Label>
                                <Input
                                    type="select"
                                    name="select"
                                    id="selectPlayerNb"
                                    className="input-group-alternative"
                                    value={maxPlayers}
                                    onChange={e => setMaxPlayers(e.target.value)}
                                >
                                    {setSelectOptions(4, 13)}
                                </Input>
                                <FormText color="muted">
                                    Séléctionnez le nombre de joueurs maximum pour cette partie.
                                </FormText>
                            </FormGroup>
                        </Col>
                        <Col md="6">
                            <FormGroup>
                                <Label className="text-uppercase font-weight-bold" htmlFor="selectPlayerNb">Nombre de pièces<small> facultatif</small></Label>
                                <Input
                                    type="select"
                                    name="select"
                                    id="selectPlayerNb"
                                    className="input-group-alternative"
                                    value={startCoins}
                                    onChange={e => setStartCoins(e.target.value)}
                                >
                                    {setSelectOptions(1, 12)}
                                </Input>
                                <FormText color="muted">
                                    Par défaut, chaque joueur à 5 pièces au début de la partie. Pour pimenter le jeu, vous pouvez choisir de changer la quantité de pièces que chaque joueur aura au début de la partie.
                                </FormText>
                            </FormGroup>
                        </Col>
                    </Row>
                </Form>
                <Button color="navylight" size="lg" block type="button" className="text-uppercase" disabled={!isValid} onClick={handleSubmit}>
                    {isValid ? 'Créer la partie' : 'Choisissez au moins un nom de partie'}
                </Button>
            </Card>
        </div>
    );
};

const selector = ({ player: { username } }) => ({ username });
export default connect(selector)(NewGame);

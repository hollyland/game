import React from 'react';
import { connect } from 'react-redux';
import { Button, Col, Row } from 'reactstrap';
import { CHANGE_GAME_STATUS } from '../../store/types';

const GameEndedScreen = ({ game, player, dispatch }) => {
    return (
        <div>
            <Row className="justify-content-center align-items-center pt-5 text-center">
                <Col>
                    <h1 className="display-1 mb-2 text-uppercase">Partie terminée</h1>
                    {game.winners.length === 1 && <h1 className="display-3 mb-5 text-uppercase">{game.winners[0].username} a gagné !</h1>}
                    {game.winners.length > 1 && <h1 className="display-3 mb-5 text-uppercase">
                        {game.winners.map(winner => ( winner.username + ' ' ))}
                        sont à égalité !
                    </h1>}
                </Col>
            </Row>
            <Row className="justify-content-center align-items-center text-center pt-5">
                <Col>
                    <h5 className="mb-5 text-uppercase">Tu as gagné {game.players.find(p => p.username === player.username).score} points d'XP</h5>
                    <Button color="navylight" className="mt-5" onClick={() => dispatch({ type: CHANGE_GAME_STATUS, payload: { gameStatus: 1 } })}>Retourner au menu</Button>
                </Col>
            </Row>
        </div>
    );
};

const selector = ({ game, player }) => ({ game, player });
export default connect(selector)(GameEndedScreen);

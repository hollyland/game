import React, { useEffect, useState } from 'react';
import { Modal, Row, Col, ModalBody } from 'reactstrap';
import GameCard from '../gameCard';

const StartModal = ({ players, isOpen, onTimerDone, timer = 0 }) => {
    /* Définition des états du composant */
    const [seconds, setSeconds] = useState(timer);
	const [open, setOpen] = useState(isOpen);
    const toggle = () => setOpen(!open);

    /* Gestion du timer */
    useEffect(() => {
        let interval = null;

        /* Si le timer n'est pas terminé, on continue sinon on termine l'interval */
        if (timer && Number(timer) && seconds > 0) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
        } else if (seconds === 0) {
            /* On appel la fonction passée au composant par le parent */
            onTimerDone();
            /* On stop l'interval */
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [seconds]);

	return (
		<div>
            <Modal className="modal-lg" isOpen={isOpen} toggle={toggle} centered>
                <ModalBody className="p-5">
                    <Row>
                        <Col>
                            <h3 className="text-center text-uppercase"><b>Essayez de retenir les cartes de chaque joueur</b></h3>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h5 className="text-center text-uppercase mb-2">Encore {seconds} seconde{seconds > 1 ? 's' : ''}</h5>
                        </Col>
                    </Row>
                    <Row className="mt-4">
                        {players.map((player, key) => (
                            <Col sm="6" md="5" lg="3" className="text-center" key={key}>
                                <h5 className="text-uppercase font-weight-bold mb-0">{player.username}</h5>
                                <GameCard card={player.card.img.default}></GameCard>
                            </Col>
                        ))}
                    </Row>
                </ModalBody>
            </Modal>
		</div>
	);
};

export default StartModal;

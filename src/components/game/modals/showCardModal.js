import React, { useEffect, useState } from 'react';
import { Modal, Row, Col, ModalBody } from 'reactstrap';
import GameCard from '../gameCard';

const ShowCardModal = ({ card, isOpen, onTimerDone, timer = 0 }) => {
    /* Définition des états du composant */
    const [seconds, setSeconds] = useState(timer);
	const [open, setOpen] = useState(isOpen);
    const toggle = () => setOpen(!open);

    /* Gestion du timer */
    useEffect(() => {
        let interval = null;

        /* Si le timer n'est pas terminé, on continue sinon on termine l'interval */
        if (timer && Number(timer) && seconds > 0) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
        } else if (seconds === 0) {
            /* On appel la fonction passée au composant par le parent */
            onTimerDone();
            /* On stop l'interval */
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [seconds]);

	return (
		<div>
            <Modal className="modal-lg" isOpen={isOpen} toggle={toggle} centered>
                <ModalBody>
                    <Row>
                        <Col>
                            <h3 className="text-center text-uppercase mb-2"><b>Voir ma carte</b></h3>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="6" lg="3" className="pb-1 text-center">
                            <GameCard card={card.img.default} />
                        </Col>
                    </Row>
                    <Row>
                        <Col className="text-center">{seconds}</Col>
                    </Row>
                </ModalBody>
            </Modal>
		</div>
	);
};

export default ShowCardModal;

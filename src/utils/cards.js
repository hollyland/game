const JUDGE = {
    name: 'Juge',
    img: require('../assets/img/card_juge.png')
};
const BISHOP = {
    name: 'Évêque',
    img: require('../assets/img/card_eveque.png')
};
const KING = {
    name: 'Roi',
    img: require('../assets/img/card_roi.png')
};
const FOOL = {
    name: 'Fou',
    img: require('../assets/img/card_fou.png')
};
const QUEEN = {
    name: 'Reine',
    img: require('../assets/img/card_reine.png')
};
const THIEF = {
    name: 'Voleur',
    img: require('../assets/img/card_voleur.png')
};
const WITCH = {
    name: 'Sorcière',
    img: require('../assets/img/card_sorciere.png')
};
const SPY = {
    name: 'Espionne',
    img: require('../assets/img/card_espionne.png')
};
const FARMER = {
    name: 'Paysan',
    img: require('../assets/img/card_paysan.png')
};
const CHEATER = {
    name: 'Tricheur',
    img: require('../assets/img/card_tricheur.png')
};
const INQUISITOR = {
    name: 'Inquisiteur',
    img: require('../assets/img/card_inquisiteur.png')
};
const WIDOW = {
    name: 'Veuve',
    img: require('../assets/img/card_veuve.png')
};

export const JUDGE_NAME = 'Juge';
export const BISHOP_NAME = 'Évêque';
export const KING_NAME = 'Roi';
export const FOOL_NAME = 'Fou';
export const QUEEN_NAME = 'Reine';
export const THIEF_NAME = 'Voleur';
export const WITCH_NAME = 'Sorcière';
export const SPY_NAME = 'Espionne';
export const FARMER_NAME = 'Paysan';
export const CHEATER_NAME = 'Tricheur';
export const INQUISITOR_NAME = 'Inquisiteur';
export const WIDOW_NAME = 'Veuve';

/* Permet de récupérer les cartes jouables en fonction du nombre de joueur */
export function getGameCards(nbPlayers) {
    switch (nbPlayers) {
        case 4:
            return [JUDGE, BISHOP, KING, QUEEN, CHEATER, THIEF];
        case 5:
        case 6:
            return [JUDGE, BISHOP, KING, QUEEN, WITCH, CHEATER];
        case 7:
            return [JUDGE, BISHOP, KING, QUEEN, THIEF, WITCH, SPY];
        case 8:
            return [JUDGE, BISHOP, KING, FOOL, QUEEN, WITCH, FARMER, FARMER];
        case 9:
            return [JUDGE, BISHOP, KING, FOOL, QUEEN, WITCH, FARMER, FARMER, CHEATER];
        case 10:
            return [JUDGE, BISHOP, KING, FOOL, QUEEN, THIEF, WITCH, SPY, FARMER, FARMER, CHEATER];
        case 11:
            return [JUDGE, BISHOP, KING, FOOL, QUEEN, WITCH, SPY, FARMER, FARMER, CHEATER, INQUISITOR];
        case 12:
            return [JUDGE, BISHOP, KING, FOOL, QUEEN, WITCH, SPY, FARMER, FARMER, CHEATER, INQUISITOR, WIDOW];
        case 13:
            return [JUDGE, BISHOP, KING, FOOL, QUEEN, THIEF, WITCH, SPY, FARMER, FARMER, CHEATER, INQUISITOR, WIDOW];
    }
};

/* Permet de distribuer aléatoirement les cartes aux joueurs */
export function shuffleAndDisturb(cards, players) {
    let playersWithCard = [];

    for (let i = cards.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [cards[i], cards[j]] = [cards[j], cards[i]];
    }

    for (let i = 0; i < players.length; i++) {
        playersWithCard[i] = {
            username: players[i],
            card: cards[i]
        };
    }

    return playersWithCard;
}

/* Récupérer les cartes non utilisées (dans le cas où on a 4 ou 5 joueurs) */
export function getUnusedCards(players, cards) {
    let usedCards = [];
    players.forEach(player => {
        usedCards.push(player.card);
    });
    return cards.filter(card => !usedCards.some(usedCard => usedCard.name === card.name));
}

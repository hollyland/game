export const CHANGE_GAME_STATUS = 'change_game_status';
export const NEW_GAME = 'new_game';
export const BACK_TO_GAME = 'back_to_game';
export const SET_PLAYER = 'set_player';
export const SET_WINNERS = 'set_winners';
export const REMOVE_PLAYER = 'remove_player';
export const UPDATE_PLAYERS = 'update_players';

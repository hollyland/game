import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Card, CardTitle, Col, ListGroup, ListGroupItem, Row } from 'reactstrap';

const GameRoundInfo = ({
    playingPlayer,
    action,
    exchangeRequired,
    round,
    previousPlayerAction,
    onClick,
    username
}) => (
    <Card body className="mt-4">
        {round >= 1 && <CardTitle className="text-uppercase text-center font-weight-bold mb-4">
            {playingPlayer.username === username ? 'à toi de jouer' : `au tour de ${playingPlayer.username}`}
        </CardTitle>}
        {round > 1 && action === 0 && playingPlayer.username === username && <Row className="mb-3">
            <Col>
                <h3 className="text-uppercase font-weight-bold text-center">Choisis une action</h3>
                <Button block color="navylight" className="mt-0" onClick={() => onClick(1)}>Échanger sa carte - ou pas</Button>
                <Button block color="navylight" className="mt-2" onClick={() => onClick(2)} disabled={exchangeRequired ? true : false}>Regarder sa carte</Button>
                <Button block color="navylight" className="mt-2" onClick={() => onClick(3)} disabled={exchangeRequired ? true : false}>Annoncer un personnage</Button>
                <hr />
            </Col>
        </Row>}
        {round > 1 && <Row>
            <Col>
                {previousPlayerAction && <ListGroup id="messages" className="text-center">
                    {previousPlayerAction.action === 1 && previousPlayerAction.victim === username && <ListGroupItem>
                        <b>{previousPlayerAction.trader} a peut être échangé sa carte avec vous.</b>
                    </ListGroupItem>}
                    {previousPlayerAction.action === 1 && previousPlayerAction.victim !== username && <ListGroupItem>
                        <b>{previousPlayerAction.trader} a fait un échange (ou pas)...</b>
                    </ListGroupItem>}
                    {previousPlayerAction.action === 2 && previousPlayerAction.username !== username && <ListGroupItem>
                        <b>{previousPlayerAction.username} a regardé sa carte.</b>
                    </ListGroupItem>}
                </ListGroup>}
            </Col>
        </Row>}
        {round === 1 && <Row>
            <Col>
                <h3 className="text-center text-uppercase">Tour d'échange</h3>
                <p className="text-center">Tous les joueurs doivent échanger (ou pas) leur carte au premier tour.</p>
            </Col>
        </Row>}
    </Card>
);

const selector = ({ player: { username } }) => ({ username });
export default connect(selector)(GameRoundInfo);

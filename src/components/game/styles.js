export const GAME_CARD_STYLES = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '10rem',
    height: '15rem',
    boxShadow: '-3px 3px 14px -3px rgba(0,0,0,0.75)',
    cursor: 'pointer'
};

export const COURT_IMAGE_STYLES = {
    background: 'transparent',
    boxShadow: '-3px 3px 14px -3px rgba(0,0,0,0.75)',
};

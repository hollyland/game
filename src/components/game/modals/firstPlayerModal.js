import React, { useEffect, useState } from 'react';
import { Modal, Row, Col, ModalBody } from 'reactstrap';

const FirstPlayerModal = ({ username, isOpen, onTimerDone, timer = 0 }) => {
    /* Définition des états du composant */
    const [seconds, setSeconds] = useState(timer);
	const [open, setOpen] = useState(isOpen);
    const toggle = () => setOpen(!open);

    /* Gestion du timer */
    useEffect(() => {
        let interval = null;

        /* Si le timer n'est pas terminé, on continue sinon on termine l'interval */
        if (timer && Number(timer) && seconds > 0) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
        } else if (seconds === 0) {
            /* On appel la fonction passée au composant par le parent */
            onTimerDone();
            /* On stop l'interval */
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [seconds]);

	return (
		<div>
            <Modal className="modal-lg" isOpen={isOpen} toggle={toggle} centered>
                <ModalBody className="p-5">
                    <Row>
                        <Col>
                            <h1 className="text-center text-uppercase mb-2">C'est <b>{username}</b> qui commence</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h4 className="text-center text-uppercase mb-2">début dans {seconds} seconde{seconds > 1 ? 's' : ''}</h4>
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>
		</div>
	);
};

export default FirstPlayerModal;

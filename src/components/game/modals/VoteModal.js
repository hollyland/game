import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Button, Modal, ModalBody, Row, Col } from 'reactstrap';
import { gameSocket } from '../../../utils';

const VoteModal = ({ isOpen, isAnnouncer, vote, nbPlayers, players, username, game }) => {
	const [open, setOpen] = useState(isOpen);
    const toggle = () => setOpen(!open);
    const [voted, setVoted] = useState(false);
    const [votesCount, setVotesCount] = useState(nbPlayers - 1);

    useEffect(() => {
        gameSocket.on('voted', (votesRemaining) => {
            setVotesCount(votesRemaining);
        });

        return () => {
            gameSocket.off('voted');
          };
    }, []);

    const handleOnVote = (choice) => {
        setVoted(true);
        gameSocket.emit('emit_voted', game.gameId, players.find(player => player.username === username), choice, vote);
    };

	return (
		<div>
            <Modal isOpen={isOpen} toggle={toggle} centered>
                {!isAnnouncer && <ModalBody>
                    <Row>
						<Col className="text-justify">
                            <h3 className="text-center tex-uppercase mb-2"><b>{vote?.player?.username}</b> a annoncé être <b>{vote?.card?.name}</b></h3>
							Vous pouvez contester si vous pensez avoir la carte annoncée.
                            Si c'est le cas, vous pourrez effectuer l'action de votre carte.
                            Si vous contester et que vous n'avez pas la bonne carte, vous serez contraint de payer une amende et d'échanger votre carte quand ça sera votre tour.
						</Col>
					</Row>
                    <Row>
                        {!voted && <Col>
                            <Button block color="navy" type="button" onClick={() => handleOnVote(1)}>Ne pas contester</Button>
                            <Button block color="navy" type="button" onClick={() => handleOnVote(2)}>Contester</Button>
                        </Col>}
                        {voted && <Col>
                            <h3 className="text-center text-uppercase mt-4">En attente des {votesCount} votes restants...</h3>
                        </Col>}
                    </Row>
                </ModalBody>}
                {isAnnouncer && <ModalBody>
                    <Row>
                        <Col>
                            <h3 className="text-center text-uppercase mt-4">Les autres joueurs votent. {votesCount} restants.</h3>
                        </Col>
                    </Row>
                </ModalBody>}
            </Modal>
		</div>
	);
};

const selector = ({ game, player: { username } }) => ({ game, username });
export default connect(selector)(VoteModal);

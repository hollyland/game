import React, { useEffect, useState } from 'react';
import { Modal, Row, Col, ModalBody } from 'reactstrap';

const PlayerLeftModal = ({ playerLeft, isOpen, timer = 0 }) => {
    /* Définition des états du composant */
    const [seconds, setSeconds] = useState(timer);
	const [open, setOpen] = useState(isOpen);
    const toggle = () => setOpen(!open);

    /* Gestion du timer */
    useEffect(() => {
        let interval = null;

        /* Si le timer n'est pas terminé, on continue sinon on termine l'interval */
        if (timer && Number(timer) && seconds > 0) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
        } else if (seconds === 0) {
            /* TODO */

            /* On stop l'interval */
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [seconds]);

	return (
		<div>
            <Modal className="modal-lg" isOpen={isOpen} toggle={toggle} centered>
                <ModalBody>
                    <Row>
                        <Col>
                            <h2 className="text-center text-uppercase mb-2"><b>{playerLeft} a quitté la partie !</b></h2>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h4 className="text-center text-uppercase mb-2"><b>La partie se terminera dans quelques secondes s'il ne se reconnecte pas</b></h4>
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>
		</div>
	);
};

export default PlayerLeftModal;

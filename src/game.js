import React, { useEffect, useState } from "react";
import Menu from "./components/menu";
import { connect } from "react-redux";
import WaitingRoom from "./components/waiting-room";
import { BACK_TO_GAME, SET_PLAYER } from "./store/types";
import { API_TOKEN, API_URL } from "./config";
import Axios from "axios";
import GameBoard from "./components/game";
import GameEndedScreen from "./components/game/gameEndedScreen";
import backgroundImg from './assets/img/mascarade_bg.png';

const Game = ({ user, gameStatus, dispatch }) => {
    /* Etat qui attend le retour de l'API avant d'afficher quoi que ce soit */
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        /* On enregistre le joueur dans le store */
        dispatch({ type: SET_PLAYER, payload: { user } });

        /* Appel à l'api pour savoir si le joueur est déjà dans une partie en cours pour le remettre dedans si c'est le cas */
        Axios({
            url: API_URL + 'go-back?username=' + user,
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-API-KEY": API_TOKEN
            }
        }).then(function (response) {
            const playerIsInGame = response.data.isInGame;

            /* Si le joueur est dans une partie en cours, on le remet dedans */
            if (playerIsInGame) {
                // const storedPlayers = JSON.parse(localStorage.getItem('players'));
                const game = response.data.game;
                dispatch({
                    type: BACK_TO_GAME,
                    payload: {
                        gameStatus: game.in_progress === '1' ? 2 : 1,
                        gameId: game.game_id,
                        name: game.name,
                        password: game.password,
                        maxPlayers: game.max_players,
                        startCoins: game.start_coins,
                        createdBy: game.created_by,
                        players: game.players
                    }
                });
            }

            setIsLoading(false);
        }).catch(function (error) {
            console.log(error);
        });
    }, []);

    /* Styles */
    const styles = {
        backgroundImage: `url(${backgroundImg})`,
        backgroundRepeat: 'repeat',
        backgroundAttachment: 'fixed',
        height: '100%',
        maring: 0,
        paddingRight: '5rem',
        paddingLeft: '5rem',
        paddingTop: '2rem',
        paddingBottom: '2rem'
    };

    return (
        !isLoading &&
        <div style={styles} className="d-flex align-items-center justify-content-center w-100">
            {gameStatus === 0 && <Menu />}
            {gameStatus === 1 && <WaitingRoom />}
            {gameStatus === 2 && <GameBoard />}
            {gameStatus === 3 && <GameEndedScreen />}
        </div>
    );
};

const selector = ({ game: { gameStatus } }) => ({ gameStatus });
export default connect(selector)(Game);

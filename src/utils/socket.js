import io from "socket.io-client";
import { SOCKET_URL } from "../config";

/* Connexion au serveur socket.io */
const gameSocket = io(SOCKET_URL, { pingInterval: 2000, timeout: 1000 });

/* On ping le serveur pour le garder en vie et éviter les timeout */
setInterval(() => gameSocket.emit('ping'), 15000);

export default gameSocket;

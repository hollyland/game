import React from 'react';
import { Card, CardTitle } from 'reactstrap';

const GameInfos = ({ game }) => (
    <Card body>
        <CardTitle className="text-uppercase text-muted mb-4">
            #{game.gameId}
        </CardTitle>
        <span className="h5 font-weight-bold mb-0">
            <small className="text-uppercase text-muted font-weight-bold mr-2">Nom de la partie</small>
            {game.name}<br />
        </span>
        <span className="h5 font-weight-bold mb-0">
            <small className="text-uppercase text-muted font-weight-bold mr-2">Type de partie</small>
            {game.password ? 'privée' : 'publique'}<br />
        </span>
        <span className="h5 font-weight-bold mb-0">
            <small className="text-uppercase text-muted font-weight-bold mr-2">Limite de joueurs</small>
            {game.maxPlayers} joueurs<br />
        </span>
        <span className="h5 font-weight-bold mb-0">
            <small className="text-uppercase text-muted font-weight-bold mr-2">Nombre de pièce au départ</small>
            {game.startCoins} pièces<br />
        </span>
    </Card>
);

export default GameInfos;

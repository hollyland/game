const path = require('path');

module.exports = {
    entry: ['babel-polyfill', './src/index.js'],
    output: {
        path: path.resolve( __dirname, 'dist' ),
        filename: 'mascarade.js',
        publicPath: 'https://games.hollyland.fun/preprod/mascarade/',
        library: 'mascarade',
        libraryTarget: 'this',
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                    presets: ['@babel/preset-env',
                                '@babel/react',{
                                'plugins': ['@babel/plugin-proposal-class-properties']}]
                    }
                }
            },
            {
                test: /\.s[ac]ss$/i,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
            {
                test: /\.(png|j?g|svg|gif)?$/,
                use: 'file-loader'
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: 'file-loader',
            }
        ]
    },
    devServer: {
        contentBase: './demo',
        watchContentBase: true,
    }
  };
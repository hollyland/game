import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, Row, Col } from 'reactstrap';
import { getGameCards } from '../../../utils';
import GameCard from '../gameCard';

const ChooseCardModal = ({ isOpen, nbPlayers, onChoice }) => {
	const [open, setOpen] = useState(isOpen);
	const toggle = () => setOpen(!open);

	return (
		<div>
			<Modal className="modal-lg" isOpen={isOpen} toggle={toggle} centered>
				<ModalHeader>Choix du personnage</ModalHeader>
				<ModalBody>
					<Row>
						<Col>
							Choisissez le personnage que vous voulez annoncer.<br />
							Vous pouvez annoncer être un personnager que vous n'êtes pas mais attention : les autres joueurs peuvent contester !
						</Col>
					</Row>
					<Row>
						{getGameCards(nbPlayers).map((card, key) => (
							<Col sm="6" lg="4" className="text-center my-2" key={key}>
								<GameCard card={card.img.default} onClick={() => onChoice(card)}></GameCard>
							</Col>
						))}
					</Row>
				</ModalBody>
			</Modal>
		</div>
	);
}

export default ChooseCardModal;
import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { Table, Button, Modal, ModalBody, Form, FormGroup, Label, Input, Alert, Card, Row, Col, CardBody } from 'reactstrap';
import { API_TOKEN, API_URL } from '../../config';
import { NEW_GAME } from '../../store/types';
import { decrypt } from '../../utils';
import { connect } from 'react-redux';

const JoinGame = ({ dispatch }) => {
    /* Définition des états du composant */
    const [reachableGames, setReachableGames] = useState({});
    const [modal, setModal] = useState(false);
    const [selectedGame, setSelectedGame] = useState(null);
    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState(false);

    useEffect(() => {
        /* Fonction qui fait l'appel à l'API qui retourne la liste des parties joignables */
        async function getReachableGames() {
            await Axios({
                url: API_URL + 'games/reachable',
                method: 'GET',
                headers: {
                    "X-API-KEY": API_TOKEN
                }
            }).then(({ data: games }) => {
                setReachableGames(games);
            }).catch((error) => {
                console.log(error);
            });
        }
        /* On récupère les parties rejoignables une première fois */
        getReachableGames();
        /* On crée un interval qui va récupérer les parties joignables toutes les 5 secondes pour actualiser le tableau */
        const reachableGamesInterval = setInterval(() => getReachableGames(), 5000);
        /* On termine l'interval lorsque l'on quitte le composant */
        return () => clearInterval(reachableGamesInterval);
    }, []);

    /* Ouvrir la modal pour renseigner le mot de passe si la partie est privée */
    useEffect(() => {
        if (selectedGame !== null) {
            if (selectedGame.password) {
                toggleModal();
            } else {
                handlePlayerJoin();
            }
        }
    }, [selectedGame])

    /* Ouvrir/Fermer la modal */
    const toggleModal = () => setModal(!modal);

    /* Mise à jour de la partie séléctionnée */
    const handleJoinButton = (game) => { setSelectedGame(game); };

    /* Décrypter le mot de passe de la partie privée et validation de ce dernier */
    const handleSubmitPassword = () => {
        if (password === decrypt(reachableGames[selectedGame.gameId].password)) {
            handlePlayerJoin();
        } else {
            setPasswordError(true);
        }
    };

    /* Mettre à jour le store pour indiquer que le joueur à rejoint la partie */
    const handlePlayerJoin = () => {
        dispatch({
            type: NEW_GAME,
            payload: {
                gameStatus: 1,
                gameId: selectedGame.gameId,
                name: selectedGame.name,
                password: selectedGame.password,
                maxPlayers: selectedGame.maxPlayers,
                startCoins: selectedGame.startCoins,
                createdBy: selectedGame.createdBy
            }
        });
    };

    return (
        <Row>
            <Col sm="12">
                <Card body className="mb-2">
                    <h1 className="display-4 text-uppercase text-center mb-0">Rejoindre une partie</h1>
                </Card>
                <Card body>
                    <Table hover borderless>
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Nom de la partie</th>
                                <th>Pièces au départ</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {Object.keys(reachableGames).map((gameId, index) => {
                                const game = reachableGames[gameId];
                                return (
                                    <tr key={index}>
                                        <td>{game.gameId}</td>
                                        <td>{game.name}</td>
                                        <td>{game.startCoins}</td>
                                        <td>{game.password ? 'Privée' : 'Publique'}</td>
                                        <td><Button color="navy" size="sm" block onClick={() => handleJoinButton(game)}>Rejoindre</Button></td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </Table>
                    <Modal isOpen={modal} toggle={toggleModal} className='bg-navy' centered>
                        <ModalBody>
                            <h4 className='text-uppercase font-weight-bold mb-4'>Partie privée</h4>
                            <Form>
                                <FormGroup>
                                    <Label className="text-uppercase font-weight-bold">Entrez le mot de passe</Label>
                                    <Alert color="danger" hidden={!passwordError}>Ce n'est pas le bon mot de passe !</Alert>
                                    <Input
                                        className="form-control-alternative"
                                        type="password"
                                        value={password}
                                        onChange={e => setPassword(e.target.value)}
                                    />
                                </FormGroup>
                            </Form>
                            <Button color="navy" type="button" onClick={handleSubmitPassword}>Rejoindre</Button>{' '}
                            <Button color="navy" onClick={toggleModal}>Cancel</Button>
                        </ModalBody>
                    </Modal>
                </Card>
            </Col>
        </Row>
    );

};

export default connect()(JoinGame)

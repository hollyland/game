import React, { useState } from 'react';
import { Button, Row, Col } from 'reactstrap';
import NewGame from './newGame';
import JoinGame from './joinGame';

const Menu = () => {
    /* 0 = boutons ; 1 = création ; 2 = rejoindre */
    const [state, setState] = useState(0);

    return (
        <div className="w-100">
            {state === 0 && <Row className="justify-content-center text-center">
                <Col sm="6" lg="3">
                    <h1 className="display-1 mb-5">Mascarade</h1>
                    <Button block color="navylight" size="lg" type="button" onClick={() => setState(1)}>Créer une partie</Button>
                    <Button block color="navylight" size="lg" type="button" onClick={() => setState(2)}>Rejoindre une partie</Button>
                </Col>
            </Row>}
            {state === 1 && <Row className="justify-content-center">
                <Col sm="12" lg="8">
                    <Button color="white" type="button" onClick={() => setState(0)} className="mb-2">Revenir au menu principal</Button>
                    <NewGame />
                </Col>
            </Row>}
            {state === 2 && <Row className="justify-content-center">
                <Col sm="12" lg="8">
                    <Button color="white" type="button" onClick={() => setState(0)} className="mb-2">Revenir au menu principal</Button>
                    <JoinGame />
                </Col>
            </Row>}
        </div>
    )
};

export default Menu;

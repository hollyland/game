import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Modal, Row, Col, ModalBody } from 'reactstrap';
import { doCardAction, gameSocket } from '../../../utils';
import GameCard from '../gameCard';

const VoteDoneModal = ({ isOpen, result, players, courtCoins, banqueCoins, actionDone, timer = 0, game, username }) => {
    /* Définition des états du composant */
    const [seconds, setSeconds] = useState(timer);
	const [open, setOpen] = useState(isOpen);
    const toggle = () => setOpen(!open);

    /* Gestion du timer */
    useEffect(() => {
        let interval = null;

        /* Si le timer n'est pas terminé, on continue sinon on termine l'interval */
        if (timer && Number(timer) && seconds > 0) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
        } else if (seconds === 0) {
            /* On appel la fonction passée au composant par le parent */
            if (result.initialVote.player.username === username) doAction();
            else actionDone();
            /* On stop l'interval */
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [seconds]);

    /* Faire l'action du personnage */
    const doAction = () => {
        let updatedPlayers = players;
        let updatedCourtCoins = courtCoins;
        let updatedBanqueCoins = banqueCoins;
        let requestExchangeList = [];
        let gameWinner = null;

        /* Executer l'action */
        if (!result.voteWinner) {
            /* Tous les joueurs qui ont contestés et celui a annoncé paient l'amende */
            const globalPenalityResult = handleGlobalPenality(updatedPlayers, updatedCourtCoins);
            updatedPlayers = globalPenalityResult.updatedPlayers;
            updatedCourtCoins = globalPenalityResult.updatedCourtCoins;
        } else {
            /* Score */
            updatedPlayers.find(player => player.username === result.voteWinner.username).score += 250;

            /* Joueur applique l'action */
            const actionResult = doCardAction(result, updatedPlayers, updatedBanqueCoins, updatedCourtCoins, gameWinner);

            /* Récupération des perdants */
            const loosersResult = handleLoosers(result, actionResult.updatedPlayers, actionResult.updatedCourtCoins);

            /* Mise à jour des variables */
            updatedPlayers = loosersResult.updatedPlayers;
            updatedCourtCoins = loosersResult.updatedCourtCoins;
            updatedBanqueCoins = actionResult.updatedBanqueCoins;
            gameWinner = actionResult.gameWinner;
            requestExchangeList = loosersResult.requestExchangeList;
            requestExchangeList.push(result.voteWinner.username);
        }

        /* Tourner au composant parent */
        actionDone();

        /* Envoyer au socket les informations mises à jour pour que les autres joueurs les recoivent */
        gameSocket.emit('get_next_player', game.gameId, { updatedPlayers, updatedCourtCoins: updatedCourtCoins.toString(), updatedBanqueCoins }, false, { requestExchangeList, gameWinner });
    };

    /* Gestion des joueurs ayant perdu au vote */
    const handleLoosers = (result, players, updatedCourtCoins) => {
        const requestExchangeList = [];

        /* On vérifie s'il y a des contestants */
        if (result.contestants.length >= 0) {
            /* On fait payer l'amende aux perdants */
            players.map(player => {
                if ((result.contestants.map(contestant => contestant.username).includes(player.username) || result.initialVote.player.username === player.username) && player.username !== result.voteWinner.username) {
                    player.coins--;
                    updatedCourtCoins++;
                }
                return player;
            });

            /* On cherche le joueur qui va être obligé d'échanger sa carte lorsque ça sera son tour */
            for (let i = 0; i < players.length; i++) {
                if (players[i].username === result.initialVote.player.username) {
                    if (i === players.length-1) {
                        if (result.contestants.map(contestant => contestant.username).includes(players[0].username)) {
                            requestExchangeList.push(players[0].username);
                        }
                    } else {
                        if (result.contestants.map(contestant => contestant.username).includes(players[i+1].username)) {
                            requestExchangeList.push(players[i+1].username);
                        }
                    }
                }
            }
        }

        return { updatedPlayers: players, updatedCourtCoins, requestExchangeList };
    };

    /* Faire payer l'amende à tous les joueurs ayant participé au vote */
    const handleGlobalPenality = (players, updatedCourtCoins) => {
        players.map(player => {
            if (result.contestants.map(contestant => contestant.username).includes(player.username) || result.initialVote.player.username === player.username) {
                player.coins--;
                updatedCourtCoins++;
            }
            return player;
        });

        return { updatedPlayers: players, updatedCourtCoins };
    }

	return (
		<div>
            <Modal className="modal-lg" isOpen={isOpen} toggle={toggle} centered>
                <ModalBody>
                    <Row>
                        <Col>
                            <h3 className="text-center text-uppercase mb-2"><b>Révélation des cartes</b></h3>
                        </Col>
                    </Row>
                    {result && result.contestants.length > 0 && <Row>
                        <Col className={result.voteWinner?.username === result.initialVote.player.username ? 'bg-navy pb-1 rounded' : 'pb-1 ' }>
                            <h1 className="text-center text-uppercase font-weight-bold mb-0">{result.initialVote.player.username}</h1>
                            <GameCard card={result.initialVote.player.card.img.default} />
                        </Col>
                        {result.contestants.map((player, key) => (
                            <Col key={key} className={result.voteWinner?.username === player.username ? 'bg-navy pb-1 rounded' : 'pb-1' }>
                                <h1 className="text-center text-uppercase font-weight-bold mb-0">{player.username}</h1>
                                <GameCard card={player.card.img.default} />
                            </Col>
                        ))}
                    </Row>}
                    {result.voteWinner && result.contestants.length > 0 && <Row className="mt-2">
                        <Col>
                            <h4 className="text-center text-uppercase mb-2"><b>{result.voteWinner.username}</b> possède la carte !</h4>
                            <h5 className="text-center text-uppercase mb-2">Il effectue donc l'action de la carte : {result.initialVote.card.name}</h5>
                            <h5 className="text-center text-uppercase mb-2">Les autres joueurs paient 1 pièce d'or d'amende</h5>
                        </Col>
                    </Row>}
                    {result.voteWinner && result.contestants.length === 0 && <Row className="mt-2">
                        <Col>
                            <h4 className="text-center text-uppercase mb-2"><b>Personne n'a contesté !</b></h4>
                            <h5 className="text-center text-uppercase mb-2"><b>{result.initialVote.player.username}</b> effectue donc l'action de la carte : {result.initialVote.card.name}</h5>
                        </Col>
                    </Row>}
                    {!result.voteWinner && <Row className="mt-2">
                        <Col>
                            <h4 className="text-center text-uppercase mb-2"><b>Personne ne possède la bonne carte !</b></h4>
                            <h5 className="text-center text-uppercase mb-2">Tout ceux qui ont contestés paient 1 pièce d'or d'amende</h5>
                        </Col>
                    </Row>}
                    <Row>
                        <Col className="text-center">{seconds}</Col>
                    </Row>
                </ModalBody>
            </Modal>
		</div>
	);
};

const selector = ({ game, player: { username } }) => ({ game, username });
export default connect(selector)(VoteDoneModal);

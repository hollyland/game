import {
    BISHOP_NAME,
    CHEATER_NAME,
    JUDGE_NAME,
    KING_NAME,
    QUEEN_NAME,
    THIEF_NAME,
    WITCH_NAME
} from "./cards";

export function doCardAction(result, updatedPlayers, updatedBanqueCoins, updatedCourtCoins, gameWinner) {
    switch (result.initialVote.card.name) {
        case JUDGE_NAME:
            updatedPlayers.find(player => player.username === result.voteWinner.username).coins += updatedCourtCoins;
            updatedCourtCoins = 0;
            break;

        case BISHOP_NAME:
            const richestPlayer = updatedPlayers.sort((p1, p2) => {
                return p2.coins - p1.coins;
            })[0];
            updatedPlayers.find(player => player.username === richestPlayer.username).coins = updatedPlayers.find(player => player.username === result.voteWinner.username).coins;
            updatedPlayers.find(player => player.username === result.voteWinner.username).coins = richestPlayer.coins;
            break;

        case KING_NAME:
            updatedPlayers.find(player => player.username === result.voteWinner.username).coins += 3;
            updatedBanqueCoins -= 3;
            break;

        case QUEEN_NAME:
            updatedPlayers.find(player => player.username === result.voteWinner.username).coins += 2;
            updatedBanqueCoins -= 2;
            break;

        case CHEATER_NAME:
            if (result.voteWinner.coins >= 10) {
                gameWinner = result.voteWinner;
            }
            break;

        case WITCH_NAME:
            let winnerIndex = null;
            let randomIndex = null;
            for (let i = 0; i < updatedPlayers.length; i++) {
                if (updatedPlayers[i].username === result.voteWinner.username) {
                    winnerIndex = i;
                    return;
                }
            }
            while (randomIndex === null || randomIndex === winnerIndex) {
                randomIndex = Math.floor(Math.random() * (updatedPlayers.length - 0 + 1) + 0);
            }
            const coinsToExchange = updatedPlayers[randomIndex].coins;
            updatedPlayers[randomIndex].coins = updatedPlayers.find(player => player.username === result.voteWinner.username).coins;
            updatedPlayers.find(player => player.username === result.voteWinner.username).coins = coinsToExchange;
            break;

        case THIEF_NAME:
            let prevPlayer = null;
            let nextPlayer = null;
            for (let i = 0; i < updatedPlayers.length; i++) {
                if (updatedPlayers[i].username === result.voteWinner.username) {
                    if (i === 0) {
                        prevPlayer = updatedPlayers[updatedPlayers.length - 1];
                        nextPlayer = updatedPlayers[i + 1]
                    } else if (i === updatedPlayers.length - 1) {
                        prevPlayer = updatedPlayers[i - 1];
                        nextPlayer = updatedPlayers[0]
                    } else {
                        prevPlayer = updatedPlayers[i - 1];
                        nextPlayer = updatedPlayers[i + 1]
                    }

                    if (prevPlayer.coins > 0) {
                        updatedPlayers.find(player => player.username === prevPlayer.username).coins --;
                        updatedPlayers[i].coins ++;
                    }

                    if (nextPlayer.coins > 0) {
                        updatedPlayers.find(player => player.username === nextPlayer.username).coins --;
                        updatedPlayers[i].coins ++;
                    }
                }
            }
            break;
    }

    return { updatedPlayers, updatedBanqueCoins, updatedCourtCoins, gameWinner };
};

import React, { useState } from 'react';
import { Card, CardImg, CardImgOverlay } from 'reactstrap';
import ReactCardFlip from 'react-card-flip';
import imgCardBack from '../../assets/img/card_back.png';

const GameCard = ({
    card,
    flippable = false,
    flipped,
    mini = false,
    onClick
}) => {
    const [isFlipped, setIsFlipped] = useState(flipped);

    /* Fonction pour retourner la carte */
    const flipCard = (e) => {
        e.preventDefault();
        setIsFlipped(!isFlipped);
    };

    /* Styles */
    const styles = {
        mini: {
            width: '50%'
        }
    }

    return (
        <div>
            {flippable && <ReactCardFlip isFlipped={isFlipped} flipDirection="vertical">
                <Card inverse onClick={(e) => flipCard(e)}>
                    <CardImg src={imgCardBack} alt="Card" style={mini ? styles.mini : {}} />
                    <CardImgOverlay />
                </Card>
                <Card inverse onClick={(e) => flipCard(e)}>
                    <CardImg src={card} alt="Card" style={mini ? styles.mini : {}} />
                    <CardImgOverlay />
                </Card>
            </ReactCardFlip>}

            {!flippable && <Card inverse style={mini ? styles.mini : {}} onClick={onClick}>
                <CardImg src={card} alt="Card" />
                <CardImgOverlay />
            </Card>}
        </div>
    );
};

export default GameCard;

import React from 'react';
import { Card, CardBody, CardTitle } from 'reactstrap';

const WaitingPlayers = ({ players }) => (
    <Card body>
        <CardTitle className="text-uppercase text-muted mb-4">Joueurs dans le salon</CardTitle>
        {players.map((player, i) => {
            return (
                <span key={i} className="h4 font-weight-bold mb-0">{player}<br /></span>
            );
        })}
    </Card>
);

export default WaitingPlayers;

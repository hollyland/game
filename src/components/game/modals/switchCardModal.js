import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Modal, Row, Col, ModalBody } from 'reactstrap';
import imgCardBack from '../../../assets/img/card_back.png';
import { gameSocket } from '../../../utils';
import GameCard from '../gameCard';

const SwitchCardModal = ({ isOpen, myCard, players, unusedCards, game, username, actionDone }) => {
	const [open, setOpen] = useState(isOpen);
    const toggle = () => setOpen(!open);

     /* Gestion de l'action "Echanger ma carte" */
     const switchCard = (choosenCard) => {
        let updatedPlayers = players;
        let prevPlayerAction = { action: 1 };

        /* Echanger les cartes */
        updatedPlayers.forEach((player, index) => {
            if (player.username === username) {
                prevPlayerAction = { ...prevPlayerAction, trader: player.username };
                updatedPlayers[index].card = choosenCard;
                updatedPlayers[index].score += 150;
            }
            if (player.card.name === choosenCard.name && player.username !== username) {
                prevPlayerAction = { ...prevPlayerAction, victim: player.username };
                updatedPlayers[index].card = myCard;
            }
        });

        /* Tourner au composant parent */
        actionDone(choosenCard);

        /* Envoyer au socket les informations mises à jour pour que les autres joueurs les recoivent */
        gameSocket.emit('get_next_player', game.gameId, { updatedPlayers }, false, { prevPlayerAction });
    };

	return (
		<div>
            <Modal isOpen={isOpen} toggle={toggle} className="modal-lg" centered>
                <ModalBody className="p-5 text-center">
                    <Row>
                        <Col>
                            <h3 className="text-center text-uppercase">
                                <b>Échanger ma carte</b>
                            </h3>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h5 className="text-center text-uppercase mb-2">
                                Clique sur la carte que tu veux garder : la tienne ou celle d'un autre joueur
                            </h5>
                        </Col>
                    </Row>
                    <Row className="justify-content-center p-5">
                        <Col sm="6" md="5" lg="3" className="text-center mb-3" key="c-me">
                            <h5 className="text-uppercase font-weight-bold mb-0">Ta carte</h5>
                            <GameCard card={imgCardBack} onClick={() => switchCard(myCard)} />
                        </Col>
                        {players.map((player, key) => (
                            player.username !== username && <Col sm="6" md="5" lg="3" className="text-center mb-3" key={`p-${key}`}>
                                <h5 className="text-uppercase font-weight-bold mb-0">{player.username}</h5>
                                <GameCard card={imgCardBack} onClick={() => switchCard(player.card)} />
                            </Col>
                        ))}
                        {unusedCards.map((card, key) => (
                            <Col sm="6" md="5" lg="3" className="text-center mb-3" key={`c-${key}`}>
                                <h5 className="text-uppercase font-weight-bold mb-0">Pioche</h5>
                                <GameCard card={imgCardBack} onClick={() => switchCard(card)} />
                            </Col>
                        ))}
                    </Row>
                </ModalBody>
            </Modal>
		</div>
	);
};

const selector = ({ game, player: { username } }) => ({ game, username });
export default connect(selector)(SwitchCardModal);

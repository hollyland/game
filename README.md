**Mascarade**
version: v0.1.0

## INSTALLATION ##

Faire la commande `npm install` pour installer les dépendances

## DEMARRAGE ##

Commandes:

* `npm start` - démarrer le serveur de dev
* `npm build` - compiler la solution en .js pour intégration n'importe où

import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { gameSocket, getGameCards, shuffleAndDisturb } from '../../utils';
import { CHANGE_GAME_STATUS, UPDATE_PLAYERS } from '../../store/types';
import { Button, Row, Col } from 'reactstrap';
import GameInfos from './gameInfos';
import WaitingPlayers from './waitingPlayers';
import Axios from 'axios';
import { API_TOKEN, API_URL } from '../../config';
import ChatRoom from '../chat';

const WaitingRoom = ({ game, username, dispatch }) => {
    /* Définition des états du composant */
    const [players, setPlayers] = useState([]);

    useEffect(() => {
        /* On écoute le socket et on met à jour la liste des joueurs en conséquence */
        gameSocket.on('player_joined', newList => setPlayers(newList));
        gameSocket.on('player_left', newList => setPlayers(newList));

        /* On prévient le socket que le joueur a rejoint la partie */
        gameSocket.emit('join_game', { gameId: game.gameId, username });

        /* Quand la partie commence, on met à jour le store */
        gameSocket.on('game_started', (players) => {
            // localStorage.setItem('players', JSON.stringify(players));
            dispatch({ type: UPDATE_PLAYERS, payload: { players } });
            dispatch({ type: CHANGE_GAME_STATUS, payload: { gameStatus: 2 } });
        });
    }, []);

    /* On veut mettre à jour le store et prévenir le socket qu'on a quitté la partie */
    const handleLeftGame = () => {
        gameSocket.emit('left_game');
        if (username === game.createdBy) {
            Axios({
                url: API_URL + 'delete-game?gameId=' + game.gameId,
                method: 'GET',
                headers: {
                    "X-API-KEY": API_TOKEN
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
        dispatch({ type: CHANGE_GAME_STATUS, payload: { gameStatus: 0 } });
    };

    /* Gestion du démarrage de la partie */
    const handleStartGame = () => {
        /* Liste des joueurs dans la salle d'attente */
        const playersToSave = players.slice(1);

        /* Appel à l'API pour enregistrer la partie et les joueurs */
        Axios({
            url: API_URL + 'game/start',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "X-API-KEY": API_TOKEN
            },
            data: {
                gameId: game.gameId,
                players: playersToSave
            }
        }).then(() => {
            /* Distribution aléatoire des cartes */
            const playersWithCard = shuffleAndDisturb(getGameCards(players.length), players);

            /* Ajout des pièces à chaque joueur */
            const computedPlayers = playersWithCard.map((player) => {
                return { ...player, coins: game.startCoins, score: 0 };
            });

            /* On prévient le socket qu'on débute la partie et on met à jour le store */
            gameSocket.emit('start_game', game.gameId, computedPlayers);
            dispatch({ type: UPDATE_PLAYERS, payload: { players: computedPlayers } });
            dispatch({ type: CHANGE_GAME_STATUS, payload: { gameStatus: 2 } });
        }).catch((error) => {
            console.log(error);
        });
    };

    return (
        <Row className="w-100 justify-content-center">
            <Col md="8" lg="6">
                <GameInfos game={game} />
                <Button color="navylight" className="mt-4" onClick={() => handleLeftGame()}>Quitter le salon</Button>
                {players.length >= 4 && username === game.createdBy && <Button color="navylight" className="mt-4 text-uppercase" onClick={() => handleStartGame()}>Commencer la partie</Button>}
            </Col>
            <Col md="4" lg="3">
                <WaitingPlayers players={players} />
                <ChatRoom gameId={game.gameId} username={username} />
            </Col>
        </Row>
    );
};

const selector = ({ game, player: { username } }) => ({ game, username });
export default connect(selector)(WaitingRoom);